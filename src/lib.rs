use std::{
    io::{stdin, stdout, Error as IOError, Write},
    net::{SocketAddr, UdpSocket},
    str::{from_utf8, Utf8Error},
};

use macros::to_err;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ClientError {
    #[error("io error: {0:?}")]
    IOError(IOError),

    #[error("io error: {0:?}")]
    Utf8Error(Utf8Error),
}

to_err!(IOError to ClientError);
to_err!(Utf8Error to ClientError);

pub fn start_client(client_addr: SocketAddr, server_addr: SocketAddr) -> Result<(), ClientError> {
    let socket = UdpSocket::bind(client_addr)?;
    let mut output_buf = [0; 2048];

    loop {
        print!("<<< ");
        stdout().flush()?;
        let mut input_buf = String::new();
        stdin().read_line(&mut input_buf)?;
        let input_chunks = input_buf[..input_buf.len() - 1]
            .as_bytes()
            .chunks(2)
            .collect::<Vec<&[u8]>>();
        for chunk in input_chunks {
            socket.send_to(chunk, server_addr)?;
        }
        socket.send_to(&[0; 0], server_addr)?;
        let (amt, _source) = socket.recv_from(&mut output_buf)?;
        println!(">>> {}", from_utf8(&output_buf[..amt])?)
    }
}
