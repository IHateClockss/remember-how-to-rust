use std::net::SocketAddr;

use clap::Parser;
use remember_how_to_rust::{start_client, ClientError};

#[derive(Parser)]
struct Args {
    /// The port the client will start on will be assigned automatically by the OS if the address is 127.0.0.1:0
    #[arg(long = "client-addr", short = 'c', default_value = "127.0.0.1:0")]
    client_addr: SocketAddr,

    /// The address of the server to join
    server_addr: SocketAddr,
}

fn main() -> Result<(), ClientError> {
    let args = Args::parse();
    start_client(args.client_addr, args.server_addr)
}
