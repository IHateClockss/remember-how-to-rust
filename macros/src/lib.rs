#[macro_export]
macro_rules! to_err {
    ($err:ident to $for:ident) => {
        impl From<$err> for $for {
            fn from(value: $err) -> Self {
                $for::$err(value)
            }
        }
    };
}

#[macro_export]
macro_rules! seg_2 {
    ($verb:ident, $split_req:ident) => {{
        if $split_req.len() < 3 {
            Err(ParseRequestError::SegmentAmtMismatch(3, $split_req.len()))?
        }
        let name = from_utf8($split_req.get(1).ok_or(ParseRequestError::NameMissing)?)?.to_string();
        Ok(Request::$verb {
            name: parse_name(name)?,
        })
    }};
}
