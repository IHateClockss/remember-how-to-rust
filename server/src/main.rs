use std::net::SocketAddr;

use clap::Parser;

#[derive(Parser)]
struct Args {
    /// The port the server will start on
    server_addr: SocketAddr,
}

fn main() -> std::io::Result<()> {
    let args = Args::parse();
    println!("starting on addrress: {}", args.server_addr);
    server::start_server(args.server_addr)
}
