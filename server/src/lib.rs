use std::{
    io::Result as IOResult,
    net::{SocketAddr, UdpSocket},
    num::ParseIntError,
    str::{from_utf8, Utf8Error},
};

use macros::{seg_2, to_err};
use thiserror::Error;

pub fn start_server(addr: SocketAddr) -> IOResult<()> {
    let mut senders: Vec<SocketAddr> = vec![];
    let socket = UdpSocket::bind(addr)?;

    loop {
        let mut full_buf: Vec<u8> = Vec::new();
        loop {
            let mut buf = [0; 2048];
            let (amt, src) = socket.recv_from(&mut buf)?;
            if amt == 0 {
                break;
            }
            full_buf.extend(&buf[..amt]);
            if !senders.contains(&src) {
                senders.push(src);
            }
        }

        for sender in &senders {
            let msg = format!("{:?}", parse_request(&full_buf));
            socket.send_to(msg.as_bytes(), sender)?;
        }
    }
}

#[derive(Error, Debug)]
pub enum ParseRequestError {
    #[error("verb is missing")]
    VerbMissing,

    #[error("unknown verb: {0:?}")]
    UnknownVerb(String),

    #[error("UTF8 error: {0:?}")]
    Utf8Error(Utf8Error),

    #[error("length is missing")]
    LenMissing,

    #[error("unable to parse length")]
    ParseIntError(ParseIntError),

    #[error("lengths mismatched. length expected: {0:?}, length provided: {1:?}")]
    LengthMismatch(usize, usize),

    #[error("name missing")]
    NameMissing,

    #[error("channel missing")]
    ChannelMissing,

    #[error("invalid name: {0:?}")]
    InvalidName(String),

    #[error("segments required: {0:?}, segments found: {1:?}")]
    SegmentAmtMismatch(usize, usize),

    #[error("message missing")]
    MessageMissing,
}

to_err!(Utf8Error to ParseRequestError);
to_err!(ParseIntError to ParseRequestError);

#[derive(Debug)]
pub enum Request {
    Connect {
        name: String,
    },
    Exit {
        name: String,
    },
    Send {
        name: String,
        channel: String,
        message: String,
    },
}

pub fn parse_request(request: &[u8]) -> Result<Request, ParseRequestError> {
    let split_req = request
        .split(|charecter| charecter == &b':')
        .collect::<Vec<&[u8]>>();
    let verb = split_req.first().ok_or(ParseRequestError::VerbMissing)?;
    match *verb {
        b"c" => seg_2!(Connect, split_req),
        b"e" => seg_2!(Exit, split_req),
        b"s" => {
            let len = split_req
                .clone()
                .into_iter()
                .nth(1)
                .ok_or(ParseRequestError::LenMissing)?;
            let len = from_utf8(len)?.parse::<usize>()?;
            let name = parse_name(
                from_utf8(split_req.get(2).ok_or(ParseRequestError::NameMissing)?)?.to_string(),
            )?;
            let channel = parse_name(
                from_utf8(split_req.get(3).ok_or(ParseRequestError::ChannelMissing)?)?.to_string(),
            )?;
            let message = from_utf8(
                split_req
                    .get(4..)
                    .ok_or(ParseRequestError::MessageMissing)?
                    .join(&b':')
                    .get(..len)
                    .ok_or(ParseRequestError::LengthMismatch(
                        len,
                        split_req
                            .get(4..)
                            .ok_or(ParseRequestError::MessageMissing)?
                            .join(&b':')
                            .len(),
                    ))?,
            )?
            .to_string();
            Ok(Request::Send {
                name,
                channel,
                message,
            })
        }
        _ => Err(ParseRequestError::UnknownVerb(from_utf8(verb)?.to_string())),
    }
}

fn parse_name(name: String) -> Result<String, ParseRequestError> {
    if name.clone().trim().is_empty() {
        Err(ParseRequestError::InvalidName(name.clone()))?
    }
    for charecter in name.clone().chars() {
        match charecter {
            ':' | '\u{0000}'..='\u{001F}' | '\u{007F}'..='\u{009F}' => {
                Err(ParseRequestError::InvalidName(name.clone()))?
            }
            _ => (),
        }
    }
    Ok(name)
}
